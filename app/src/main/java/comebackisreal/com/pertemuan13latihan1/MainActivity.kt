package comebackisreal.com.pertemuan13latihan1

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var dataSpinner = arrayOf("Google", "Yahoo", "Facebook","Instagram")
    var notifier_channel1 = 1
    var notifier_channel2 = 2
    var notificationManager: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, dataSpinner)
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner!!.setAdapter(myAdapter)

        radioGroup.check(radioButton1.id)

        notificationManager =  getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        createNotificationGroups()
        createNotificationChannels()

        btnNotification.setOnClickListener {
            if (pesan.getText().toString().length > 0) {
                var channel_id = "";
                var group_id = "";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if(radioButton1.isChecked)
                        group_id = radioButton1.getText().toString();
                    else
                        group_id = radioButton2.getText().toString();

                    channel_id = notificationManager!!.getNotificationChannel(spinner.getSelectedItem().toString()+ "_" + group_id).getId();
                }
                var notification = NotificationCompat.Builder(this, channel_id)
                    .setContentTitle(spinner.getSelectedItem().toString())
                    .setContentText(pesan.getText().toString())
                    .setGroup(group_id)
                    .setSmallIcon(R.mipmap.ic_launcher)

                if(radioButton1.isChecked)
                    notificationManager!!.notify(notifier_channel1, notification.build())
                else
                    notificationManager!!.notify(notifier_channel2, notification.build())

            } else {
                Toast.makeText(getApplicationContext(), "EditText tidak boleh kosong", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createNotificationGroups() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val list =  mutableListOf<NotificationChannelGroup>()
            list.add(NotificationChannelGroup(radioButton1.getText().toString(), radioButton1.getText()))
            list.add(NotificationChannelGroup(radioButton2.getText().toString(), radioButton2.getText()))
            notificationManager!!.createNotificationChannelGroups(list)
        }
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val att = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            for (s in dataSpinner) {
                val Channel1 = NotificationChannel(s + "_" + radioButton1.getText().toString(), s, NotificationManager.IMPORTANCE_HIGH)
                Channel1.group = radioButton1.getText().toString()
                Channel1.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 100)
                Channel1.setSound(notificationSound,att);
                Channel1.setLightColor(Color.RED);
                Channel1.enableLights(true)
                Channel1.enableVibration(true)
                //tambahan
                Channel1.setShowBadge(true)

                val Channel2 = NotificationChannel(s + "_" + radioButton2.getText().toString(), s, NotificationManager.IMPORTANCE_NONE)
                Channel2.group = radioButton2.getText().toString()
                Channel2.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 100)
                Channel2.enableLights(true)
                Channel2.enableVibration(true)
                //tambahan
                Channel2.setShowBadge(true)

                if (notificationManager != null) {
                    notificationManager!!.createNotificationChannel(Channel1)
                    notificationManager!!.createNotificationChannel(Channel2)
                }

            }
        }
    }

}
